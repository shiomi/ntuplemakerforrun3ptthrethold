/*
<<<<<<< HEAD
 *   Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
 *   */

#include <iostream>
#include <iomanip>

#include "TGCNSW.h"
#include "TGCNSWOut.h"


//////////////////////
TGCNSW::TGCNSW()
{
  for (int side=0; side < 2; side++) {
    for (int TP=0; TP < NumberOfNSWTriggerProcesser; TP++) {
      std::vector<int> vecTP;
      vecTP.push_back(TP);
      buffer[side][TP] = new TGCNSWOut(side, vecTP); 
    }
  }
}

//////////////////////
TGCNSW::~TGCNSW()
{
  for(int idx1=0; idx1<2; idx1++){
    for (int idx2=0; idx2<NumberOfNSWTriggerProcesser; idx2++){
      delete buffer[idx1][idx2]; 
    }
  }
}

///////////////////////////////////////////////////////////////
TGCNSW::TGCNSW(const TGCNSW& right)
{
  for(int idx1=0; idx1<2; idx1++){
    for (int idx2=0; idx2<NumberOfNSWTriggerProcesser; idx2++){
      buffer[idx1][idx2] = 0; 
    }
  }
  *this= right;
}


/////////////////////////////////////////////////////////////
TGCNSW& TGCNSW::operator=(const TGCNSW& right)
{
  if (this != &right) {
    for(int  idx1=0; idx1<2; idx1++){
      for (int idx2=0; idx2<NumberOfNSWTriggerProcesser; idx2++){
        delete buffer[idx1][idx2];
        buffer[idx1][idx2] = new TGCNSWOut(*(right.buffer[idx1][idx2]));
      }
    }
  }
  return *this;
}

/////////////////////////////////////////////////////////////
const TGCNSWOut* TGCNSW::getOutput(TGCRegionType region ,int side,int TGC_TriggerSector) const
{
  TGCNSWOut *tirgNSW_output=new TGCNSWOut();
  tirgNSW_output->Clear();
  if ( (side<0)||(side>1) ) return 0;
  if(region==ENDCAP){
    if ( TGC_TriggerSector<0 || TGC_TriggerSector>47 ) return 0;
    ////NSW Large Secor Info
    int NSW_TriggerSecot=(TGC_TriggerSector+1)/6*2;
    if(NSW_TriggerSecot==16){NSW_TriggerSecot=0;}
    if(NSW_TriggerSecot>=0 && NSW_TriggerSecot<=15){
      *tirgNSW_output+=*buffer[side][NSW_TriggerSecot];
    }

    ////NSW Small Sector Info
    if(TGC_TriggerSector%6==1 || TGC_TriggerSector%6==2){
      return tirgNSW_output;
    }
    NSW_TriggerSecot=(TGC_TriggerSector+1+(TGC_TriggerSector-1)/6*2)/4;
    if(NSW_TriggerSecot==0){NSW_TriggerSecot=15;}
    if(NSW_TriggerSecot>=0 && NSW_TriggerSecot<=15){
      *tirgNSW_output+=*buffer[side][NSW_TriggerSecot];
    }

    return tirgNSW_output;
  }

  if(region==FORWARD){
    if ( TGC_TriggerSector<0 || TGC_TriggerSector>23 ) return 0;
    ////NSW Large Secor Info
    int NSW_TriggerSecot=(TGC_TriggerSector+1)/3*2;
    if(NSW_TriggerSecot==16){NSW_TriggerSecot=0;}
    if(NSW_TriggerSecot>=0 && NSW_TriggerSecot<=15){
      *tirgNSW_output+=*buffer[side][NSW_TriggerSecot];
    }

    ///NSW Small Sector Info
    NSW_TriggerSecot=2*(TGC_TriggerSector+2)/3-1;
    if(NSW_TriggerSecot==-1){NSW_TriggerSecot=15;}
    if(NSW_TriggerSecot>=0 && NSW_TriggerSecot<=15){
      *tirgNSW_output+=*buffer[side][NSW_TriggerSecot];
    }
    return tirgNSW_output;
  } 


  return 0;  
}


/////////////////////////////////////////////////////////////
void  TGCNSW::setOutput(int side, int NSWTriggerProcesser, int NSWeta_8bit, int NSWphi_6bit, int NSWDtheta_5bit)
{
  if ( (side<0)||(side>1) ) return;//side 1::Aside 0::Cside
  if ( (NSWTriggerProcesser<0) || (NSWTriggerProcesser>=NumberOfNSWTriggerProcesser) ) return;
  buffer[side][NSWTriggerProcesser]->SetSide(side);
  buffer[side][NSWTriggerProcesser]->SetNSWTriggerProcessor(NSWTriggerProcesser);
  buffer[side][NSWTriggerProcesser]->SetEta(NSWeta_8bit);
  buffer[side][NSWTriggerProcesser]->SetPhi(NSWphi_6bit);
  buffer[side][NSWTriggerProcesser]->SetDtheta(NSWDtheta_5bit);
}

/////////////////////////////////////////////////////////////
void  TGCNSW::eraseOutput()
{
  for(int idx1=0; idx1<2; idx1++){
    for (int idx2=0; idx2<NumberOfNSWTriggerProcesser; idx2++){
      buffer[idx1][idx2]->Clear(); 
    }
  }
}

/////////////////////////////
void TGCNSW::Print() const
{
  for(int idx1=0; idx1<2; idx1++){
    for (int idx2=0; idx2<NumberOfNSWTriggerProcesser; idx2++){
      buffer[idx1][idx2]->Print(); 
    }
  }
}
  


