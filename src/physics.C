#define physics_cxx
#include "physics.h"

#include "TGCRPhiCoincidenceMap.h"
#include "TGCNSWCoincidenceMap.h"
#include "TGCNSW.h"
#include "HistogramManager.h"
#include "RoIObj.h"

#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TTree.h>
#include <TLorentzVector.h>

void physics::Loop(std::string BWCW_path, std::string NSWCW_path)
{

    //////////////////////
    //// INITIALIZE
    ////////////////////


    std::string hsName="CW_MC_Bsmumu_NewCW";
    //std::string hsName="TEST";
    HistogramManager hs(hsName);

    //defined in  MuonTrigCoinData/​TgcCoinData.h
    enum {
        TYPE_TRACKLET,
        TYPE_HIPT,
        TYPE_SL,
        TYPE_UNKNOWN,
        TYPE_TRACKLET_EIF};

    enum {NumberOfSide=2, NumberOfOctant=8};
    TGCRPhiCoincidenceMap *bwMap[NumberOfSide][NumberOfOctant];
    TGCNSWCoincidenceMap *nswMap;

    if(BWCW_path!=""){
        for(int i_side=0;i_side!=NumberOfSide;i_side++){
            for(int j_octant=0;j_octant!=NumberOfOctant;j_octant++){
                bwMap[i_side][j_octant] = new TGCRPhiCoincidenceMap(BWCW_path,i_side,j_octant);
            }
        }
    }

    //if(NSWCW_path!=""){nswMap= new TGCNSWCoincidenceMap(NSWCW_path);}


    if (fChain == 0) return;

    Long64_t nentries = fChain->GetEntriesFast();

    Long64_t nbytes = 0, nb = 0;



    //////////////////////
    ////  EVENT LOOP
    /////////////////////

    for (Long64_t jentry=0; jentry<nentries;jentry++) {
    //for (Long64_t jentry=0; jentry<500000;jentry++) {
        Long64_t ientry = LoadTree(jentry);
        if (ientry < 0) break;
        nb = fChain->GetEntry(jentry);   nbytes += nb;


        if(jentry%100000==0){  
            std::cout<<"JOBS..."<<(double)jentry/10000<<"%..COMPLETE"<<std::endl;
        }
        if(TGC_coin_n==0){continue;}

        hs.muon_pt->clear();
        hs.muon_phi->clear();
        hs.muon_eta->clear();
        hs.muon_m->clear();
        hs.muon_author->clear();
        hs.muon_charge->clear();
        hs.muon_Type->clear();
        hs.ext_mu_type->clear();
        hs.ext_mu_index->clear();
        hs.ext_mu_size->clear();
        hs.ext_mu_targetVec->clear();
        hs.ext_mu_targetDistanceVec->clear();
        hs.ext_mu_targetEtaVec->clear();
        hs.ext_mu_targetPhiVec->clear();
        hs.ext_mu_targetDeltaEtaVec->clear();
        hs.ext_mu_targetDeltaPhiVec->clear();
        hs.ext_mu_targetPxVec->clear();
        hs.ext_mu_targetPyVec->clear();
        hs.ext_mu_targetPzVec->clear();
        hs.tgc_coin_x_In->clear();
        hs.tgc_coin_y_In->clear();
        hs.tgc_coin_z_In->clear();
        hs.tgc_coin_x_Out->clear();
        hs.tgc_coin_y_Out->clear();
        hs.tgc_coin_z_Out->clear();
        hs.tgc_coin_width_In->clear();
        hs.tgc_coin_width_Out->clear();
        hs.tgc_coin_width_R->clear();
        hs.tgc_coin_width_Phi->clear();
        hs.tgc_coin_isAside->clear();
        hs.tgc_coin_isStrip->clear();
        hs.tgc_coin_isForward->clear();
        hs.tgc_coin_isInner->clear();
        hs.tgc_coin_type->clear();
        hs.tgc_coin_trackletId->clear();
        hs.tgc_coin_trackletIdStrip->clear();
        hs.tgc_coin_phi->clear();
        hs.tgc_coin_roi->clear();
        hs.tgc_coin_pt->clear();
        hs.tgc_coin_delta->clear();
        hs.tgc_coin_sub->clear();
        hs.tgc_coin_veto->clear();
        hs.tgc_coin_bunch->clear();
        hs.tgc_coin_inner->clear();
        hs.muctpi_eta->clear();
        hs.muctpi_phi->clear();
        hs.muctpi_source->clear();
        hs.muctpi_hemisphere->clear();
        hs.muctpi_bcid->clear();
        hs.muctpi_sectorID->clear();
        hs.muctpi_thrNumber->clear();
        hs.muctpi_roi->clear();
        hs.muctpi_veto->clear();
        hs.muctpi_charge->clear();
        hs.muctpi_candidateVetoed->clear();
        hs.HLT_info_chain->clear();
        hs.HLT_info_isPassed->clear();
        hs.HLT_info_typeVec->clear();
        hs.HLT_info_ptVec->clear();
        hs.HLT_info_etaVec->clear();
        hs.HLT_info_phiVec->clear();
        hs.mc_pt->clear();
        hs.mc_eta->clear();
        hs.mc_phi->clear();
        hs.mc_m->clear();
        hs.mc_charge->clear();
        hs.TGC_Run3_pt->clear();
        hs.TGC_Run3_type->clear();
        hs.TGC_Run3_station->clear();
        hs.TGC_Run3_DR->clear();
        hs.TGC_Run3_DPhi->clear();
        hs.TGC_Run3_TypeDPhi->clear();
        hs.TGC_Run3_TypeDR->clear();
        hs.TGC_Run3_Side->clear();
        hs.TGC_Run3_RoI->clear();
        hs.TGC_Run3_PhiSector->clear();
        hs.TGC_Run3_IsEndcap->clear();
        hs.TGC_Run3_TrackletIdWire->clear();
        hs.TGC_Run3_TrackletIdStrip->clear();
        hs.TGC_Run3_x->clear();
        hs.TGC_Run3_y->clear();
        hs.TGC_Run3_z->clear();
        hs.TGC_Run3_R->clear();
        hs.TGC_Run3_Phi->clear();
        hs.TGC_Run3_Charge->clear();


        /////////////////////
        /// Fill RoI Info
        ////////////////////
        std::vector<RoIObj> RoIs;
        int Run3_n=0;
        for(int i=0;i!=TGC_coin_n;i++){
            if((*TGC_coin_type)[i]!=TYPE_SL){continue;}
            if((*TGC_coin_bunch)[i]!=2){continue;}//1 = previous, 2 = current, 3 = next 
            RoIObj roi_tmp;
            roi_tmp.setRoI((*TGC_coin_roi)[i]);
            roi_tmp.setType((*TGC_coin_type)[i]);
            roi_tmp.setIsEndcap(!(*TGC_coin_isForward)[i]);
            roi_tmp.setSide(!(*TGC_coin_isAside)[i]);
            roi_tmp.setTrackletIdWire((*TGC_coin_trackletId)[i]);
            roi_tmp.setTrackletIdStrip((*TGC_coin_trackletIdStrip)[i]);
            roi_tmp.setX((*TGC_coin_x_Out)[i]);
            roi_tmp.setY((*TGC_coin_y_Out)[i]);
            roi_tmp.setZ((*TGC_coin_z_Out)[i]);
            roi_tmp.setR((*TGC_coin_width_R)[i]);
            roi_tmp.setPhi((*TGC_coin_width_Phi)[i]);
            roi_tmp.setPhiSector((*TGC_coin_phi)[i]);

            RoIs.push_back(roi_tmp);
        }
        Run3_n=RoIs.size();

        /////////////////////////
        ////   Fill DR/DPhi Info
        /////////////////////////

        for(int i=0;i!=TGC_coin_n;i++){
            for(auto &roi : RoIs){
                if((*TGC_coin_type)[i]==TYPE_SL || (*TGC_coin_type)[i]==TYPE_UNKNOWN || (*TGC_coin_type)[i]==TYPE_TRACKLET_EIF){continue;}
                if((*TGC_coin_bunch)[i]!=2){continue;}//1 = previous, 2 = current, 3 = next 
                if((*TGC_coin_phi)[i] != roi.getPhiSector()){continue;}

                if((*TGC_coin_trackletId)[i] == roi.getTrackletIdStrip() && (*TGC_coin_isStrip)[i]){
                    if(roi.getTypeDPhi() <= (*TGC_coin_type)[i]){
                        if((*TGC_coin_type)[i]==TYPE_HIPT){if((*TGC_coin_width_Out)[i]!=roi.getPhi()){continue;}}
                        roi.setTypeDPhi((*TGC_coin_type)[i]);
                        roi.setDPhi((*TGC_coin_delta)[i]);
                    }
                } // dPhi
                if((*TGC_coin_trackletId)[i] == roi.getTrackletIdWire()  && !(*TGC_coin_isStrip)[i]){
                    if(roi.getTypeDR() <= (*TGC_coin_type)[i]){
                        if((*TGC_coin_type)[i]==TYPE_HIPT){if((*TGC_coin_width_Out)[i]!=roi.getR()){continue;}}
                        roi.setTypeDR((*TGC_coin_type)[i]);
                        roi.setDR((*TGC_coin_delta)[i]);
                    }
                } //DR 
            }
        }

        //////////////////
        /// calc pT
        //////////////////

        for(auto &roi : RoIs){
            int pT = bwMap[roi.getSide()][roi.getOctant()]->test_Run3(roi.getOctant(), roi.getModuleId(),roi.getRoI(),roi.getCoinType(),roi.getDR(),roi.getDPhi());
            roi.setPt(std::abs(pT));
            if(pT<0){roi.setCharge(0);} //Charge = -
            else if(pT>0){roi.setCharge(1);} //Charge = +
            else {roi.setCharge(2);} //Charge = unknown
        }

        /*
            for(int j=0;j<148;j++){
                for(int k=-15;k!=16;k++){
                    for(int l=-7;l!=8;l++){
                        int pT = bwMap[0][0]->test_Run3(0,3,j,0,k,l);
                        h_CW[j]->Fill(l,k,pT);
                    }
                }
            }
        Int_t palette[31] = {46,45,44,42,41,29,22,30,32,31,37,36,34,38,39,10,51,54,58,62,65,69,73,76,80,84,87,90,93,96,99};
        gStyle->SetPalette(31,palette);

        TString pdf = "./SideID=0_SourceID=1_SectorId=2_TEST.pdf";
        TCanvas *c1 = new TCanvas("c1","c1",450,750);
        c1->Print(pdf + "[", "pdf");
        for(int j=0;j<148;j++){
            h_CW[j]->Draw("L COL text");
            h_CW[j]->SetMarkerColor(0);
            c1->SetGrid();
            c1->Print(pdf,"pdf");
        }
        c1 -> Print( pdf + "]", "pdf" );
        delete c1;
        */


        /////////////////
        /// Fill Ntuple
        ////////////////

        //EventInformation
        hs.eventnumber = EventNumber;
        hs.bcid = bcid;

        //Offline
        hs.muon_n = mu_n;
        for(int i=0;i!=mu_n;i++){
            hs.muon_pt->push_back((*mu_pt)[i]);
            hs.muon_eta->push_back((*mu_eta)[i]);
            hs.muon_phi->push_back((*mu_phi)[i]);
            hs.muon_m->push_back((*mu_m)[i]);
            hs.muon_charge->push_back((*mu_charge)[i]);
            hs.muon_author->push_back((*mu_author)[i]);
            hs.muon_Type->push_back((*mu_muonType)[i]);
        }

        //Extrapolate
        hs.ext_mu_n = ext_mu_bias_n;
        for(int i=0;i!=ext_mu_bias_n;i++){
            hs.ext_mu_type->push_back((*ext_mu_bias_type)[i]);
            hs.ext_mu_index->push_back((*ext_mu_bias_index)[i]);
            hs.ext_mu_size->push_back((*ext_mu_bias_size)[i]);
            std::vector<int> targetVec;
            std::vector<float> DistanceVec;
            std::vector<float> EtaVec;
            std::vector<float> PhiVec;
            std::vector<float> DeltaEtaVec;
            std::vector<float> DeltaPhiVec;
            std::vector<float> PxVec;
            std::vector<float> PyVec;
            std::vector<float> PzVec;
            for(int j=0;j!=ext_mu_bias_targetVec->at(i).size();j++){
                targetVec.push_back(ext_mu_bias_targetVec->at(i).at(j));
                DistanceVec.push_back(ext_mu_bias_targetDistanceVec->at(i).at(j));
                EtaVec.push_back(ext_mu_bias_targetEtaVec->at(i).at(j));
                PhiVec.push_back(ext_mu_bias_targetPhiVec->at(i).at(j));
                DeltaEtaVec.push_back(ext_mu_bias_targetDeltaEtaVec->at(i).at(j));
                DeltaPhiVec.push_back(ext_mu_bias_targetDeltaPhiVec->at(i).at(j));
                PxVec.push_back(ext_mu_bias_targetPxVec->at(i).at(j));
                PyVec.push_back(ext_mu_bias_targetPyVec->at(i).at(j));
                PzVec.push_back(ext_mu_bias_targetPzVec->at(i).at(j));
            }
            hs.ext_mu_targetVec->push_back(targetVec);
            hs.ext_mu_targetDistanceVec->push_back(DistanceVec);
            hs.ext_mu_targetEtaVec->push_back(EtaVec);
            hs.ext_mu_targetPhiVec->push_back(PhiVec);
            hs.ext_mu_targetDeltaPhiVec->push_back(DeltaPhiVec);
            hs.ext_mu_targetDeltaEtaVec->push_back(DeltaEtaVec);
            hs.ext_mu_targetPxVec->push_back(PxVec);
            hs.ext_mu_targetPyVec->push_back(PyVec);
            hs.ext_mu_targetPzVec->push_back(PzVec);
        }

        //HLT
        hs.HLT_info_n = trigger_info_n;
        for(int i=0;i!=trigger_info_n;i++){
            hs.HLT_info_chain->push_back((*trigger_info_chain)[i]);
            hs.HLT_info_isPassed->push_back((*trigger_info_isPassed)[i]);
            std::vector<int> typeVec;
            std::vector<float> ptVec;
            std::vector<float> phiVec;
            std::vector<float> etaVec;
            for(int j=0;j!=trigger_info_typeVec->at(i).size();j++){
                typeVec.push_back(trigger_info_typeVec->at(i).at(j));
                ptVec.push_back(trigger_info_ptVec->at(i).at(j));
                phiVec.push_back(trigger_info_phiVec->at(i).at(j));
                etaVec.push_back(trigger_info_etaVec->at(i).at(j));
            }
            hs.HLT_info_typeVec->push_back(typeVec);
            hs.HLT_info_ptVec->push_back(ptVec);
            hs.HLT_info_etaVec->push_back(etaVec);
            hs.HLT_info_phiVec->push_back(phiVec);
        }

        //RoI
        hs.muctpi_ndatawords = muctpi_nDataWords;
        for(int i=0;i!=muctpi_nDataWords;i++){
            hs.muctpi_eta->push_back((*muctpi_dw_eta)[i]);
            hs.muctpi_phi->push_back((*muctpi_dw_phi)[i]);
            hs.muctpi_source->push_back((*muctpi_dw_source)[i]);
            hs.muctpi_hemisphere->push_back((*muctpi_dw_hemisphere)[i]);
            hs.muctpi_bcid->push_back((*muctpi_dw_bcid)[i]);
            hs.muctpi_sectorID->push_back((*muctpi_dw_sectorID)[i]);
            hs.muctpi_thrNumber->push_back((*muctpi_dw_thrNumber)[i]);
            hs.muctpi_roi->push_back((*muctpi_dw_roi)[i]);
            hs.muctpi_veto->push_back((*muctpi_dw_veto)[i]);
            hs.muctpi_charge->push_back((*muctpi_dw_charge)[i]);
            hs.muctpi_candidateVetoed->push_back((*muctpi_dw_candidateVetoed)[i]);
        }

        //TGC Coin Data
        hs.tgc_coin_n = TGC_coin_n;
        for(int i=0;i!=TGC_coin_n;i++){
            hs.tgc_coin_x_In->push_back((*TGC_coin_x_In)[i]);
            hs.tgc_coin_y_In->push_back((*TGC_coin_y_In)[i]);
            hs.tgc_coin_z_In->push_back((*TGC_coin_z_In)[i]);
            hs.tgc_coin_x_Out->push_back((*TGC_coin_x_Out)[i]);
            hs.tgc_coin_y_Out->push_back((*TGC_coin_y_Out)[i]);
            hs.tgc_coin_z_Out->push_back((*TGC_coin_z_Out)[i]);
            hs.tgc_coin_width_In->push_back((*TGC_coin_width_In)[i]);
            hs.tgc_coin_width_Out->push_back((*TGC_coin_width_Out)[i]);
            hs.tgc_coin_width_R->push_back((*TGC_coin_width_R)[i]);
            hs.tgc_coin_width_Phi->push_back((*TGC_coin_width_Phi)[i]);
            hs.tgc_coin_isAside->push_back((*TGC_coin_isAside)[i]);
            hs.tgc_coin_isStrip->push_back((*TGC_coin_isStrip)[i]);
            hs.tgc_coin_isForward->push_back((*TGC_coin_isForward)[i]);
            hs.tgc_coin_isInner->push_back((*TGC_coin_isInner)[i]);
            hs.tgc_coin_type->push_back((*TGC_coin_type)[i]);
            hs.tgc_coin_trackletId->push_back((*TGC_coin_trackletId)[i]);
            hs.tgc_coin_trackletIdStrip->push_back((*TGC_coin_trackletIdStrip)[i]);
            hs.tgc_coin_phi->push_back((*TGC_coin_phi)[i]);
            hs.tgc_coin_roi->push_back((*TGC_coin_roi)[i]);
            hs.tgc_coin_pt->push_back((*TGC_coin_pt)[i]);
            hs.tgc_coin_delta->push_back((*TGC_coin_delta)[i]);
            hs.tgc_coin_sub->push_back((*TGC_coin_sub)[i]);
            hs.tgc_coin_veto->push_back((*TGC_coin_veto)[i]);
            hs.tgc_coin_bunch->push_back((*TGC_coin_bunch)[i]);
            hs.tgc_coin_inner->push_back((*TGC_coin_inner)[i]);
        }

        //Run-3
        hs.TGC_Run3_n = Run3_n; 
        for(auto &roi : RoIs){
            hs.TGC_Run3_type->push_back(roi.getType());
            hs.TGC_Run3_pt->push_back(roi.getPt());
            hs.TGC_Run3_station->push_back(roi.getCoinType());
            hs.TGC_Run3_DR->push_back(roi.getDR());
            hs.TGC_Run3_DPhi->push_back(roi.getDPhi());
            hs.TGC_Run3_TypeDPhi->push_back(roi.getTypeDPhi());
            hs.TGC_Run3_TypeDR->push_back(roi.getTypeDR());
            hs.TGC_Run3_Side->push_back(roi.getSide());
            hs.TGC_Run3_RoI->push_back(roi.getRoI());
            hs.TGC_Run3_PhiSector->push_back(roi.getPhiSector());
            hs.TGC_Run3_IsEndcap->push_back(roi.getIsEndcap());
            hs.TGC_Run3_TrackletIdWire->push_back(roi.getTrackletIdWire());
            hs.TGC_Run3_TrackletIdStrip->push_back(roi.getTrackletIdStrip());
            hs.TGC_Run3_x->push_back(roi.getX());
            hs.TGC_Run3_y->push_back(roi.getY());
            hs.TGC_Run3_z->push_back(roi.getZ());
            hs.TGC_Run3_R->push_back(roi.getR());
            hs.TGC_Run3_Phi->push_back(roi.getPhi());
            hs.TGC_Run3_Charge->push_back(roi.getCharge());
        }

        //Truth
        /*
        hs.mc_n = mc_n;
        for(int i=0;i!=mc_n;i++){
            hs.mc_pt->push_back((*mc_pt)[i]);
            hs.mc_eta->push_back((*mc_eta)[i]);
            hs.mc_phi->push_back((*mc_phi)[i]);
            hs.mc_m->push_back((*mc_m)[i]);
            hs.mc_charge->push_back((*mc_charge)[i]);
        }
        */ 
        hs.m_tree->Fill();
    }
}
