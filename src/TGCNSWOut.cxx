/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

// ====================================================================
/*
        TGCNSWOut.cc
                                      
*/
// ====================================================================

#include <iostream>
#include <iomanip>

#include "TGCNSWOut.h"



// ====================================================================
//
// class description
//
// ====================================================================

//////////////////////
TGCNSWOut::TGCNSWOut()
  : sideID(-1), NSWTriggerProcessor(0), NSWeta_8bit(0), NSWphi_6bit(0), NSWDtheta_5bit(0)
//////////////////////
{
}

///////////////////////////////////////////////////////////////
  TGCNSWOut::TGCNSWOut(int side, std::vector<int> NSWTP, std::vector<int> NSWeta,std::vector<int> NSWphi, std::vector<int> NSWDtheta)
    : sideID(side), NSWTriggerProcessor(NSWTP), NSWeta_8bit(NSWeta), NSWphi_6bit(NSWphi), NSWDtheta_5bit(NSWDtheta)
///////////////////////////////////////////////////////////////
{
}
  TGCNSWOut::TGCNSWOut(int side, std::vector<int> NSWTP)
    : sideID(side), NSWTriggerProcessor(NSWTP), NSWeta_8bit(0), NSWphi_6bit(0), NSWDtheta_5bit(0)
///////////////////////////////////////////////////////////////
{
}

///////////////////////////////////////////////////////////////
TGCNSWOut::TGCNSWOut(const TGCNSWOut& right)
/////////////////////////////////////////////////////////////
    :sideID(right.sideID), 
     NSWTriggerProcessor(right.NSWTriggerProcessor),
     NSWeta_8bit(right.NSWeta_8bit),
     NSWphi_6bit(right.NSWphi_6bit),
     NSWDtheta_5bit(right.NSWDtheta_5bit)
{
}


/////////////////////////////////////////////////////////////
TGCNSWOut& TGCNSWOut::operator=(const TGCNSWOut& right)
/////////////////////////////////////////////////////////////
{
  if (this != &right) {
    sideID    = right.sideID; 
    NSWTriggerProcessor  = right.NSWTriggerProcessor;
    NSWeta_8bit    = right.NSWeta_8bit;
    NSWphi_6bit     = right.NSWphi_6bit;
    NSWDtheta_5bit     = right.NSWDtheta_5bit;
  }
  return *this;
}
 
///////////////////////////////////////////////////////////
TGCNSWOut& TGCNSWOut::operator+=(const TGCNSWOut& right)
///////////////////////////////////////////////////////////
{
  if (this != &right && (sideID==right.sideID || sideID==-1)) {
   NSWTriggerProcessor.insert(NSWTriggerProcessor.end(), right.NSWTriggerProcessor.begin(), right.NSWTriggerProcessor.end());
   NSWeta_8bit.insert(NSWeta_8bit.end(), right.NSWeta_8bit.begin(), right.NSWeta_8bit.end());
   NSWphi_6bit.insert(NSWphi_6bit.end(), right.NSWphi_6bit.begin(), right.NSWphi_6bit.end());
   NSWDtheta_5bit.insert(NSWDtheta_5bit.end(), right.NSWDtheta_5bit.begin(), right.NSWDtheta_5bit.end());
 }

 return *this;

}

/////////////////////////////
void TGCNSWOut::Print() const 
/////////////////////////////
{
  
  for(unsigned int i=0;i!=NSWTriggerProcessor.size();i++){
    std::cout<<"i="<<i<<std::endl;
    std::cout<<"Size=="<<NSWTriggerProcessor.size()<<std::endl;
    std::cout << "Side=" << sideID <<std::endl;
    std::cout<< " :: ModuleID=" << NSWTriggerProcessor[i]<<std::endl;
    std::cout<< " :: NSWeta_8bit=" << NSWeta_8bit[i]<<std::endl;
    std::cout<< " :: NSWphi_6bit=" << NSWphi_6bit[i]<<std::endl;
    std::cout<< " :: NSWDtheta_5bit=" << NSWDtheta_5bit[i]<<std::endl;
  }
  
}



