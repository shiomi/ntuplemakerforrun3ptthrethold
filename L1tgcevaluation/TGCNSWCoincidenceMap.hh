#ifndef TGCNSWCoincidenceMap_hh
#define TGCNSWCoincidenceMap_hh

#include <string>

#include "GaudiKernel/ToolHandle.h"
#include "TrigT1TGC/TGCNumbering.h"

class ITGCTriggerDbTool;



namespace LVL1TGCTrigger {

class TGCNSWOut;

class TGCNSWCoincidenceMap {

private:
  TGCNSWCoincidenceMap();// hide default constructor
  enum {N_Side=2};
  enum {N_TGCTRIGGERSECTOR_Endcap=48,N_TGCTRIGGERSECTOR_Forward=24};
  enum {N_RoI_Endcap=148,N_RoI_Forward=64};
  enum {N_dR=64};//6bit eta
  enum {N_dPHI=16};//4bit phi
  enum {N_Dtheta=32};//5bit phi
  enum {N_PT_THRESH=15};// Number of pT theshold
  enum ReadCW_Type{EtaPhi_CW=0,EtaDtheta_CW};

public:

  int TGCNSW_pTcalcu_EtaPhi(const TGCNSWOut *NSWOut,int sideId,TGCRegionType region,
                            int sectorId,int RoI) const;
  int TGCNSW_pTcalcu_EtaDtheta(const TGCNSWOut *NSWOut,int sideId,TGCRegionType region,
                               int sectorId,int RoI) const;


  const std::string& getVersion() const;
  int   getSideId() const;
  int   getOctantId() const;

  bool getFlag(int pt,int side,TGCRegionType region,int sector,int roi) const;

  TGCNSWCoincidenceMap(const std::string& version);

  virtual ~TGCNSWCoincidenceMap();

  // copy and assignment operator
  TGCNSWCoincidenceMap(const TGCNSWCoincidenceMap& right);
  bool readMap(std::string kModule[6], bool isEndcap, ReadCW_Type cw_type);
  bool readShift();


private:
  short int Endcap_CW[N_Side][N_TGCTRIGGERSECTOR_Endcap][N_RoI_Endcap][N_dR][N_dPHI];
  short int Forward_CW[N_Side][N_TGCTRIGGERSECTOR_Forward][N_RoI_Forward][N_dR][N_dPHI];
  short int Endcap_etaDthetaCW[N_Side][N_TGCTRIGGERSECTOR_Endcap][N_RoI_Endcap][N_dR][N_Dtheta];
  short int Forward_etaDthetaCW[N_Side][N_TGCTRIGGERSECTOR_Forward][N_RoI_Forward][N_dR][N_Dtheta];

  short int Endcap_Shift[N_Side][N_TGCTRIGGERSECTOR_Endcap][N_RoI_Endcap][2];//0=eta_shift 1=phi_shift
  short int Forward_Shift[N_Side][N_TGCTRIGGERSECTOR_Forward][N_RoI_Forward][2];//0=eta_shift 1=phi_shift

  bool Forward_Flag_EtaPhi[N_Side][N_TGCTRIGGERSECTOR_Forward][N_RoI_Forward][N_PT_THRESH];
  bool Endcap_Flag_EtaPhi[N_Side][N_TGCTRIGGERSECTOR_Endcap][N_RoI_Endcap][N_PT_THRESH];
  bool Forward_Flag_EtaDtheta[N_Side][N_TGCTRIGGERSECTOR_Forward][N_RoI_Forward][N_PT_THRESH];
  bool Endcap_Flag_EtaDtheta[N_Side][N_TGCTRIGGERSECTOR_Endcap][N_RoI_Endcap][N_PT_THRESH];


  std::string m_verName;
  int m_side;
  int m_octant;
  bool m_fullCW;

  ToolHandle<ITGCTriggerDbTool> m_condDbTool;
};





} //end of namespace bracket

#endif // TGCNSWCoincidenceMap_hh


