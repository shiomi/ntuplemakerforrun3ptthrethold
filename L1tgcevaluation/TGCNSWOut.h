/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef TGC_NSWOUT_H
#define TGC_NSWOUT_H

#include <vector>
#include "TGCNumbering.h"



class TGCNSWOut
{
 public:
  TGCNSWOut();
  TGCNSWOut(int side, std::vector<int> NSWTrigger, std::vector<int> NSWeta, std::vector<int> NSWphi, std::vector<int> NSWDtheta); 
  TGCNSWOut(int side, std::vector<int> NSWTrigger); 

  virtual ~TGCNSWOut() { }
 
  TGCNSWOut(const TGCNSWOut& right);
 
  TGCNSWOut& operator=(const TGCNSWOut& right);
  TGCNSWOut& operator+(const TGCNSWOut& right);
  TGCNSWOut& operator+=(const TGCNSWOut& right);
  int operator==(const TGCNSWOut& right) const
  {
    return (this==&right);
  }
 
  int operator!=(const TGCNSWOut& right) const
  {
    return (this!=&right);
  }

  void SetSide(int side){ sideID = side; }
  void SetNSWTriggerProcessor(int NSWTP){ NSWTriggerProcessor.push_back(NSWTP); }
  void SetEta(int NSWeta){ NSWeta_8bit.push_back(NSWeta); }
  void SetPhi(int NSWphi){ NSWphi_6bit.push_back(NSWphi); }
  void SetDtheta(int NSWDtheta){ NSWDtheta_5bit.push_back(NSWDtheta); }
  void Clear() { sideID=-1;NSWTriggerProcessor.clear(); NSWeta_8bit.clear();  NSWphi_6bit.clear(); NSWDtheta_5bit.clear();}

  int GetSide() const { return sideID; }
  std::vector<int> GetNSWTriggerProcessor() const {return NSWTriggerProcessor; }
  std::vector<int> GetNSWeta() const {return NSWeta_8bit; }
  std::vector<int> GetNSWphi() const {return NSWphi_6bit; }
  std::vector<int> GetNSWDtheta() const {return NSWDtheta_5bit; }

  void Print() const;

 protected:
  int            sideID=-1;     // 0:A-side 1:C-side
  std::vector<int>            NSWTriggerProcessor;   // 0..23
  std::vector<int>            NSWeta_8bit;     //  0.005
  std::vector<int>            NSWphi_6bit;      // 10mrad
  std::vector<int>            NSWDtheta_5bit;    // 1mrad

};




#endif
